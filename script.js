
"use strict";
$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gBASE_URL = "http://203.171.20.210:8080/crud-api/users/";
  const gUSER_COLS = ["id", "firstname", "lastname", "country", "subject", "customerType", "registerStatus", "action"];
  //
  const gUSER_ID_COL = 0
  const gUSER_FIRSTNAME_COL = 1
  const gUSER_LASTNAME_COL = 2
  const gUSER_COUNTRY_COL = 3
  const gUSER_SUBJECT_COL = 4
  const gUSER_CUSTOMER_TYPE_COL = 5
  const gUSER_REGISTER_STATUS_COL = 6
  const gUSER_ACTION_COL = 7
  var gDonutData = {
    labels: [
      'Chrome',
      'IE',
      'FireFox',
      'Safari',
      'Opera',
      'Navigator',
    ],
    datasets: [
      {
        data: [700, 500, 400, 600, 300, 100],
        backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
      }
    ]
  }
  var gAreaChartData = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'Digital Goods',
        backgroundColor: 'rgba(60,141,188,0.9)',
        borderColor: 'rgba(60,141,188,0.8)',
        pointRadius: false,
        pointColor: '#3b8bba',
        pointStrokeColor: 'rgba(60,141,188,1)',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data: [50, 30, 40, 60, 86, 80, 90]
      },
      {
        label: 'Electronics',
        backgroundColor: 'rgba(210, 214, 222, 1)',
        borderColor: 'rgba(210, 214, 222, 1)',
        pointRadius: false,
        pointColor: 'rgba(210, 214, 222, 1)',
        pointStrokeColor: '#c1c7d1',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(220,220,220,1)',
        data: [65, 59, 80, 81, 56, 55, 40]
      },
    ]
  }
  // Khai báo dataTable
  var gUserTable = $("#user-table").DataTable({
    // Phân trang
    paging: true,
    // Tìm kiếm
    searching: false,
    // Sắp xếp
    ordering: false,
    // Số bản ghi phân trang
    lengthChange: false,
    columns: [
      { data: gUSER_COLS[gUSER_ID_COL] },
      { data: gUSER_COLS[gUSER_FIRSTNAME_COL] },
      { data: gUSER_COLS[gUSER_LASTNAME_COL] },
      { data: gUSER_COLS[gUSER_COUNTRY_COL] },
      { data: gUSER_COLS[gUSER_SUBJECT_COL] },
      { data: gUSER_COLS[gUSER_CUSTOMER_TYPE_COL] },
      { data: gUSER_COLS[gUSER_REGISTER_STATUS_COL] },
      { data: gUSER_COLS[gUSER_ACTION_COL] },
    ],
    columnDefs: [
      { // định nghĩa lại cột action
        targets: gUSER_ACTION_COL,
        defaultContent: `<button id="btn-edit" class="btn btn-info">Sửa</button><button id="btn-delete" class ="btn btn-danger">Xóa</button>`
      }
    ]
  });
  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  onPageLoading()
  // Gán sự kiện cho nút sửa
  $(document).on("click", "#btn-edit", function () {
    onBtnSuaClick(this)
  })
  // Gán sự kiện cho nút xóa
  $(document).on("click", "#btn-delete", function () {
    onBtnXoaClick(this)

  })
  // Gán sự kiện cho nút thêm user
  $(document).on("click", "#btn-add-user", function () {
    $("#create-user-modal").modal("show")
  })
  // Gán sự kiện cho nút insert user trong modal thêm user
  $(document).on("click", "#btn-create-user", function () {
    var vUserObject = {
      firstname: "",
      lastname: "",
      subject: "",
      country: "",
      customerType: "",
      registerStatus: ""
    }
    // B1 thu thập dữ liệu
    getDataToAddUser(vUserObject);
    // B2 Kiểm tra dữ liệu
    var vValidate = isDataValidate(vUserObject)
    if (vValidate == true) {
      // B3 Gọi Api 
      processingApiToAddUser(vUserObject)
      // B4 Xử lý hiển thị

    }
  })
  // Gán sự kiện cho nút cập nhật user trong modal sửa
  $(document).on("click", "#btn-update-user", function () {
    var vUserObject = {
      firstname: "",
      lastname: "",
      subject: "",
      country: "",
      customerType: "",
      registerStatus: ""
    }
    // B1 Thu thập dữ liệu 
    getDataToUpdateUser(vUserObject)
    // B2 Kiểm tra dữ liệu
    var vValidate = isDataValidate(vUserObject)
    if (vValidate == true) {
      //B3 Gọi Api
      processingApiToUpdateUser(vUserObject)
    }
  })
  // Gán sự kiện cho nút confirm xóa User
  $(document).on("click", "#btn-confirm-delete-user", function () {
    // B1 thu thập dữ liệu [ Không có]
    // B2 Kiểm tra dữ liệu [ Không có]
    // B3 Gọi Api
    processingApiToDeleteUser()
  })
  //-------------
  //- BAR CHART -
  //-------------
  var barChartCanvas = $('#barChart').get(0).getContext('2d')
  var barChartData = $.extend(true, {}, gAreaChartData)
  var temp0 = gAreaChartData.datasets[0]
  var temp1 = gAreaChartData.datasets[1]
  barChartData.datasets[0] = temp1
  barChartData.datasets[1] = temp0

  var barChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    datasetFill: false
  }

  new Chart(barChartCanvas, {
    type: 'bar',
    data: barChartData,
    options: barChartOptions
  })
  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
  var pieData = gDonutData;
  var pieOptions = {
    maintainAspectRatio: false,
    responsive: true,
  }
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  new Chart(pieChartCanvas, {
    type: 'pie',
    data: pieData,
    options: pieOptions
  })
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // hàm thực thi khi trang được load
  function onPageLoading() {
    getAllUser();
  }
  // Hàm xử lý sự kiện nút sửa
  function onBtnSuaClick(paramBtnSua) {
    var vRowSelected = $(paramBtnSua).parents("tr")
    var vDataRow = gUserTable.row(vRowSelected).data()
    var vId = vDataRow.id
    $("#btn-update-user").attr("id-object", `${vId}`)
    $("#input-update-firstname").val(vDataRow.firstname)
    $("#input-update-lastname").val(vDataRow.lastname)
    $("#input-update-subject").val(vDataRow.subject)
    $("#select-update-country").val(vDataRow.country)
    $("#select-update-customerType").val(vDataRow.customerType)
    $("#select-update-registerStatus").val(vDataRow.registerStatus)
    $("#update-user-modal").modal("show")

  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  // Hàm get dữ liệu để điền vào table
  function getAllUser() {
    $.ajax({
      url: gBASE_URL,
      type: "GET",
      success: function (paramResponseObject) {
        //Điền dữ liệu vào table
        //console.log(paramResponseObject)
        loadDataToUserTable(paramResponseObject);
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  // Hàm điền dữ liệu vào table
  function loadDataToUserTable(paramObject) {
    gUserTable.clear();
    gUserTable.rows.add(paramObject);
    gUserTable.draw();
  }
  // Hàm thu thập dữ liệu cho việc thêm user
  function getDataToAddUser(paramObject) {
    paramObject.firstname = $.trim($("#input-create-firstname").val())
    paramObject.lastname = $.trim($("#input-create-lastname").val())
    paramObject.subject = $.trim($("#input-create-subject").val())
    paramObject.country = $.trim($("#select-create-country").val())
    paramObject.customerType = $.trim($("#select-create-customerType").val())
    paramObject.registerStatus = $.trim($("#select-create-registerStatus").val())
  }
  // Hàm kiểm tra dữ liệu cho việc thêm user
  function isDataValidate(paramObject) {
    if (paramObject.firstname == "") {
      alert("hãy nhập firstname")
      return false
    }
    if (paramObject.lastname == "") {
      alert("hãy nhập lastname")
      return false
    }
    if (paramObject.subject == "") {
      alert("hãy nhập subject")
      return false
    }
    if (paramObject.country == "") {
      alert("hãy chọn country")
      return false
    }
    if (paramObject.customerType == "") {
      alert("hãy chọn customerType")
      return false
    }
    if (paramObject.registerStatus == "") {
      alert("hãy chọn registerStatus")
      return false
    }
    return true
  }
  //hàm gọi Api xử lý thêm user lên server
  function processingApiToAddUser(paramObject) {
    $.ajax({
      url: gBASE_URL,
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(paramObject),
      type: "POST",
      success: function (ResponseObject) {
        displayAddSuccess(ResponseObject)

      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  //Hàm hiển thị khi thêm user thành công
  function displayAddSuccess(ResponseObject) {
    // Xóa trăng các Input và đặt select về giá trị ban đầu
    $("#input-create-firstname").val("")
    $("#input-create-lastname").val("")
    $("#input-create-subject").val("")
    $("#select-create-country").val("VN")
    $("#select-create-customerType").val("Standard")
    $("#select-create-registerStatus").val("New")
    $("#create-user-modal").modal("hide")
    // Hiện thông báo thêm user thành công
    alert(`
        Đã thêm user thành công 
        Id của bạn là: ${ResponseObject.id}`)
    // Load lại bảng
    getAllUser()
  }
  // Hàm thu thập dữ liệu cho việc cập nhật user
  function getDataToUpdateUser(paramObject) {
    paramObject.firstname = $.trim($("#input-update-firstname").val())
    paramObject.lastname = $.trim($("#input-update-lastname").val())
    paramObject.subject = $.trim($("#input-update-subject").val())
    paramObject.country = $.trim($("#select-update-country").val())
    paramObject.customerType = $.trim($("#select-update-customerType").val())
    paramObject.registerStatus = $.trim($("#select-update-registerStatus").val())
  }
  //hàm gọi Api xử lý thêm update lên server
  function processingApiToUpdateUser(paramObject) {
    $.ajax({
      url: gBASE_URL + $("#btn-update-user").attr("id-object"),
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(paramObject),
      type: "PUT",
      success: function (ResponseObject) {
        displayUpdateSuccess(ResponseObject)

      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  //Hàm hiển thị khi update user thành công
  function displayUpdateSuccess(ResponseObject) {
    // Xóa trăng các Input và đặt select về giá trị ban đầu
    $("#input-update-firstname").val("")
    $("#input-update-lastname").val("")
    $("#input-update-subject").val("")
    $("#select-update-country").val("VN")
    $("#select-update-customerType").val("Standard")
    $("#select-update-registerStatus").val("New")
    $("#update-user-modal").modal("hide")
    // Hiện thông báo update user thành công
    alert(`
        Đã cập nhật thành công user: ${ResponseObject.firstname} ${ResponseObject.lastname}
        `)
    // Load lại bảng
    getAllUser()
  }
  // Hàm xử lý sự kiện khi click nút xóa
  function onBtnXoaClick(paramBtnXoa) {
    $("#delete-confirm-modal").modal("show")
    // B1 Thu thập dữ liệu 
    var vRowSelected = $(paramBtnXoa).parents("tr")
    var vDataRow = gUserTable.row(vRowSelected).data()
    var vId = vDataRow.id
    $("#btn-confirm-delete-user").attr("id-object", `${vId}`)
  }
  // Hàm gọi APi để xóa user
  function processingApiToDeleteUser() {
    $.ajax({
      url: gBASE_URL + $("#btn-confirm-delete-user").attr("id-object"),
      type: "DELETE",
      success: function () {
        // Hiển thị thông báo xóa thành công
        alert("Đã xóa user thành công")
        // Ẩn modal delete
        $("#delete-confirm-modal").modal("hide")
        // Tải lại bảng
        getAllUser()

      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  //
})
